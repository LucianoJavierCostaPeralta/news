import axios from "axios";
import { createContext, useEffect, useState } from "react";

const NewsContext = createContext();

const NewsProvider = ({ children }) => {
  const [category, setCategory] = useState("general");
  const [news, setNews] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getNews = async () => {
      setLoading(true);

      const url = `https://newsapi.org/v2/top-headlines?country=us&category=${category}&apiKey=${
        import.meta.env.VITE_API_KEY
      }`;

      try {
        const { data } = await axios(url);
        setNews(data.articles);
      } catch (error) {
        console.error("Error fetching news:", error);
        setNews([]);
      }

      setLoading(false);
    };

    getNews();
  }, [category]);

  const handleChangeCategory = (e) => setCategory(e.target.value);

  return (
    <NewsContext.Provider
      value={{
        category,
        handleChangeCategory,
        news,
        loading,
      }}
    >
      {children}
    </NewsContext.Provider>
  );
};

export { NewsContext };

export default NewsProvider;
