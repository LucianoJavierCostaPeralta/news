import { Container, Grid, Typography } from "@mui/material";
import { Form } from "./components/Form";
import NewsProvider from "./context/NewsProvider";
import { ItemList } from "./components/ItemList";

function App() {
  return (
    <NewsProvider>
      <Container>
        <Typography component="h1" variant="h3" align="center" marginY={5}>
          Buscador de noticias
        </Typography>

        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
        >
          <Grid item xs={12} md={6}>
            <Form />
          </Grid>
        </Grid>

        <ItemList />
      </Container>
    </NewsProvider>
  );
}

export default App;
