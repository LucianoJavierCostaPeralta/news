import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useNews } from "../hooks/useNews";
import { CATEGORIAS } from "../constants";

export const Form = () => {
  const { category, handleChangeCategory } = useNews();

  return (
    <div>
      <form>
        <FormControl fullWidth>
          <InputLabel>Categoria</InputLabel>

          <Select
            label="categoria"
            onChange={handleChangeCategory}
            value={category}
          >
            {CATEGORIAS?.map((item) => (
              <MenuItem key={item.value} value={item.value}>
                {item.label ?? null}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </form>
    </div>
  );
};
