import {
  Card,
  CardContent,
  CardMedia,
  Grid,
  Link,
  Typography,
} from "@mui/material";

export const Item = ({ data }) => {
  const { urlToImage, url, title, source } = data;

  return (
    <Grid item xs={12} md={6} lg={4}>
      <Link
        href={url}
        sx={{
          textDecoration: "none",
          transition: "all 0.5s",
          "&:hover": {
            h5: {
              textDecoration: "underline",
            },
            img: {
              opacity: 0.8,
            },
          },
        }}
      >
        <Card>
          {urlToImage && (
            <CardMedia
              image={urlToImage ?? null}
              component="img"
              alt={`Imagen de la noticia ${title}`}
              title={title}
              height={250}
            />
          )}

          <CardContent>
            {source.name && (
              <Typography variant="body1" color="error">
                {source.name}
              </Typography>
            )}

            {title && <Typography variant="h5">{title}</Typography>}
          </CardContent>
        </Card>
      </Link>
    </Grid>
  );
};
