import { Grid, Typography } from "@mui/material";
import { useNews } from "../hooks/useNews";
import { Item } from "./Item";

export const ItemList = () => {
  const { news, loading } = useNews();

  return (
    <>
      <Typography align="center" marginY={5} variant="h4" component="h4">
        Últimas noticias
      </Typography>
      {loading ? (
        <p>loading...</p>
      ) : (
        <>
          <Grid container spacing={4}>
            {news?.map((item) => (
              <Item key={item.url} data={item} />
            ))}
          </Grid>
        </>
      )}
    </>
  );
};
